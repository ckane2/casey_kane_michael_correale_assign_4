Names: Casey Kane and Michael Correale
Emails: ckane2@binghamton.edu
	mcorrea4@binghamton.edu

----------------------------------------------------------
## To clean:
ant -buildfile src/build.xml clean

----------------------------------------------------------
## To compile:
ant -buildfile src/build.xml all

Please compile before run. Needed to get BUILD and
BUILD/classes directories in directory structure
----------------------------------------------------------
## To run by specifying arguments from command line
ant -buildfile src/build.xml run -Darg0='path/to/input.txt' -Darg1='path/to/output.txt' -Darg2='3' -Darg3='words to delete' -Darg4='0' 

----------------------------------------------------------
## To create tarball for submission
ant -buildfile src/build.xml tarzip

----------------------------------------------------------
## Academic Honesty Statement

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense."

[Date: ] -- 11/8/17

----------------------------------------------------------
## Algorithm Note

Our tree operates as a standard BST tree where nodes to the right are greater than
the parent, and nodes to the left are less than the parent. It operates in O(log(N))
time, as all BST trees do.

Our logger has 5 levels:
0 - Release: Nothing is printed, this is for running the program fully
1 - Result: Logs each time a result is written. This happens rarely, at 
    the end of the programs execution.
2 - Thread: Logs each time a thread is spawned. This happens when helper
    and/or creator threads are made
3 - Word: Logs each time a word is processed. This happens fairly often, 
    and regardless of the word was already inserted or not.
4 - Constructor: Logs each time a constructor is called.
----------------------------------------------------------
## Citations
